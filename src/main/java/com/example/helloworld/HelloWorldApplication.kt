package com.example.helloworld

import io.dropwizard.Application
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import com.example.helloworld.resources.HelloWorldResource
import com.example.helloworld.health.TemplateHealthCheck
import com.example.helloworld.resources.TaskResource
import com.example.helloworld.store.TaskStore

class HelloWorldApplication : Application<HelloWorldConfiguration>() {

    override fun getName(): String {
        return "hello-world"
    }

    override fun initialize(bootstrap: Bootstrap<HelloWorldConfiguration>?) {
        // nothing to do yet
    }

    override fun run(config: HelloWorldConfiguration,
                     environment: Environment) {
        val resource = HelloWorldResource(config.template, config.defaultName)
        val taskResource = TaskResource(TaskStore())
        val healthCheck = TemplateHealthCheck(config.template)
        environment.healthChecks().register("template", healthCheck)
        environment.jersey().register(resource)
        environment.jersey().register(taskResource)
    }

    companion object {
        @Throws(Exception::class)
        @JvmStatic fun main(args: Array<String>) {
            HelloWorldApplication().run(*args)
        }
    }

}