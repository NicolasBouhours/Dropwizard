package com.example.helloworld.store

import com.example.helloworld.api.Task

class TaskStore (val list: ArrayList<Task> = ArrayList<Task>(), var counter: Int = 0) {

    fun getTasks(): ArrayList<Task> {
        return this.list
    }

    fun getTask(id: Int) : Task? {
        val filteredTasks = this.list.filter { it -> it.id == id }
        return if (filteredTasks.isNotEmpty()) filteredTasks[0] else null
    }

    fun addTask(task: Task) : Task {
        this.counter++
        this.list.add(task)
        return task
    }

    fun deleteTask(task: Task) {
        this.list.remove(task)
    }
}

