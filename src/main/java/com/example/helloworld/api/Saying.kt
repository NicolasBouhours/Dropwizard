package com.example.helloworld.api

import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.Length

class Saying(val id: Long, @Length(max = 3) val content: String)