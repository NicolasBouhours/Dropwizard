package com.example.helloworld.resources

import com.codahale.metrics.annotation.Timed
import com.example.helloworld.api.Task
import com.example.helloworld.store.TaskStore
import java.util.*
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType

@Path("/task")
@Produces(MediaType.APPLICATION_JSON)
class TaskResource(val taskStore: TaskStore) {

    @GET
    @Timed
    fun getTasks() : ArrayList<Task> {
        return this.taskStore.getTasks()
    }

    @POST
    @Timed
    fun addTask(@QueryParam("name") name : String, @QueryParam("description") description: String?): Task {
        val task = this.taskStore.addTask(Task(this.taskStore.counter, name, description, true))
        return task
    }
}
